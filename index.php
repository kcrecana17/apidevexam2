<?php

class Contestants
{
    private $contestants;

    public function __construct($entries)
    {
        $this->contestants = $entries;
    }

    public function generate()
    {
        $storedArr = $this->contestants;
        $results = [];
        $matches = 1;
        $rounds = 0;
        $winners = [];

        // Validate
        $hasInvalidInteger = array_filter($this->contestants, function ($var) {
            return !is_numeric($var);
        });

        if ($hasInvalidInteger) {
            $results[] = "Invalid Format: Contestants should be Integers";
            return $results;
        }

        while (count($storedArr) > 0) {
            $rounds++;

            $first = reset($storedArr);
            $last = end($storedArr);

            if (count($storedArr) > 1) {
                if ($first > $last) {
                    $results[] = "Round {$rounds}: {$first} vs {$last} Winner is : {$first}";
                    $winners[] = $first;
                } else {
                    $results[] = "Round {$rounds}: {$first} vs {$last} Winner is : {$last}";
                    $winners[] = $last;
                }
            } else {
                $results[] = "Round {$rounds}: Default Winner is: {$storedArr[0]}";
                $winners[] = $storedArr[0];
            }

            $storedArr = array_slice($storedArr, 1, -1);

            if (count($storedArr) == 0 && count($winners) != 1) {
                $results[] = 'Next ...';
                $storedArr = $winners;
                $winners = [];
                $matches++;
            }
        }

        $champion = isset($winners[0]) ? $winners[0] :  '';
        $results[] = "Champion: {$champion}";
        $results[] = "Total Bracket Matches: {$matches}";
        $results[] = "Total Rounds: {$rounds}";

        return $results;
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>APIDevExam2</title>
</head>

<body>
    <form action="" method="post">
        <label>Enter the number of contestant: </label>
        <input type="number" name="contestants" min="1" max="100" />
        <button type="button" name="entries">generate entries</button>

        <div id="contestants"></div>

        <button type="submit" name="submit-contestants">Submit Contestants</button>

        <div id="simulation" style="padding-top: 1rem;">
            <?php if (isset($_POST['submit-contestants']) && isset($_POST['contestant'])) {
                echo "Enter the Number of contestant: " . count($_POST['contestant']) . "<br>";

                $count = 1;
                foreach ($_POST['contestant'] as $contestant) {
                    echo "Enter Contestant # {$count}: {$contestant} <br>";
                    $count++;
                }

                echo '<br>';
                echo 'Simulation';
                echo '<br>';

                $c = new Contestants($_POST['contestant']);
                foreach ($c->generate() as $result) {
                    echo $result . '<br>';
                }
            } ?>
        </div>
    </form>

    <script src="./jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script>
        $(function() {
            $('button[name="entries"]').on('click', function() {
                var totalContestants = $('input[name="contestants"]').val();

                $('#contestants, #simulation').html('');
                for (var a = 1; a <= totalContestants; a++) {
                    $('#contestants').append(
                        `<label>Enter Contestant # ${a}</label>` +
                        '<input type="text" name="contestant[]" />' + '<br>'
                    );
                }
            });
        });
    </script>
</body>

</html>